package org.binar.chapter7.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class StudentBinar {

    @Id
    private String id;

//    @NonNull
    private String username;

//    @NonNull
    private String kelas;

//    @NonNull
    private Integer batch;

//    @NonNull
    private String program;

//    @NonNull
    private Integer usia;
}
