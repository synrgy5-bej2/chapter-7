package org.binar.chapter7.service;

import org.binar.chapter7.model.FilesDb;

public interface IFilesDbService {
    String uploadFile(FilesDb filesDb);
    FilesDb downloadFile(Long id);
}
