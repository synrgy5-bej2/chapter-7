package org.binar.chapter7.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @Value("${spring.kafka.topic}")
    private String topicName;

//    @Value()

    @KafkaListener(topics = "test-bej2", groupId = "bej2")
    public void listenTopic(String message) {
        System.out.println("message yang didapat dari kafka : " + message);
    }
}
