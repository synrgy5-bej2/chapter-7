package org.binar.chapter7.kafka;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class KafkaProducerTest {

    @Autowired
    public KafkaProducer kafkaProducer;

    @Test
    void testProduce() {
        kafkaProducer.sendMessage("Ternyata errornya cuma gara-gara access modifier!");
    }
}
