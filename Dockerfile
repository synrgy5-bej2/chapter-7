FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} chapter7.jar
ENTRYPOINT ["java","-jar","/chapter7.jar"]
